import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';
import MatrixRowTitle from './MatrixRowTitle';

export class MatrixRadioOption extends BaseOption {
    // 选择的值
       public SelValue: string = '';
       // 填空的值
       public BlankValues: string[] = [];
       //选中的选项
       public Checkvalue:string[]=[];
}

export default class MatrixRadioSubject extends BaseQuestionSubject<MatrixRadioOption> {
    public static option: MatrixRadioOption;
    // 行标题
    public RowTitles: MatrixRowTitle[] = [];
    public RowTitleText: String = '';


}
